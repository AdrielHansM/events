<?php
require_once('../../server-side/sessionHandling.php');
//Verify if the member that logged in is an admin
checkAccountType();
//  $varPHP = $_GET['id'];
//  echo $varPHP;
include_once("../head.html");
?>
<title>Admin View Talk | KASIMANATICS-TALKS</title>
<!-- Internal CSS -->
<link rel="stylesheet" href="../css/style.css">
<!-- bootstrap CSS Files -->
<link href="../assets/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../assets/bootstrap/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
</head>

<body>
    <!-- ======= Header ======= -->
    <header id="header" class="d-flex align-items-center ">
        <div class="container-fluid container-xxl d-flex align-items-center">

            <div id="logo" class="me-auto">
                <!-- text logo -->
                <h1><a href="client-home.php">Kasimanatics<span>Talks</span></a></h1>
            </div>

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                    <li><a class="nav-link scrollto" href="#hero">Home</a></li>
                    <li><a class="nav-link scrollto" href="#about">About</a></li>
                    <li><a class="nav-link scrollto" href="#events">Events</a></li>
                    <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
                    <li class="dropdown"><a href="#"><span>My Account</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="client-event.php">Event History</a></li>
                            <li><a href="#">Drop Down 3</a></li>
                            <li><a href="#">Drop Down 4</a></li>
                            <li><a id="logout" onclick="logoutToWebsite()">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <!-- .navbar -->
            <a class="buy-tickets scrollto" href="#buy-tickets">Buy Tickets</a>

        </div>
    </header>
    <!-- End Header -->
    <main id="main" class="main-page">
        <?php
        if (isset($_GET['talkid'])) {
            $talkid =  $_GET['talkid']; // Outputs : JS says Hi! 
        }
        ?><h1><?php echo $talkid;  ?> Details</h1>
        <div class="talksDiv" id="talkDet">

        </div>

        <button type='button' onclick="" id="update-talk">Update</button>
        <button type='button' onclick="" id="delete-talk">Delete</button>
        <button type="button" onclick="location.href = 'admin-home.php';">Back</button>

    </main>
    <!-- ======= Footer ======= -->
    <footer id="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h3><a href="index.html">Kasimanatics<span>Talks</span></a></h3>
                        <p>In alias aperiam. Placeat tempore facere. Officiis voluptate ipsam vel eveniet est dolor et totam porro. Perspiciatis ad omnis fugit molestiae recusandae possimus. Aut consectetur id quis. In inventore consequatur ad voluptate
                            cupiditate debitis accusamus repellat cumque.</p>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Events</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Contact</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Legal Info</h4>
                        <ul>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Privacy Policy</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Terms & Conditions</a></li>
                            <li><i class="bi bi-chevron-right"></i> <a href="#">Cookie Policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>
                            A. Bonifacio Street <br> Baguio City, 2600<br> Philippines <br>
                            <strong>Phone:</strong> +6374 619 0367 <br>
                            <strong>Email:</strong> kasimanatics@gmail.com<br>
                        </p>

                        <div class="social-links">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End  Footer -->

    <!-- back to top -->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <!-- Animation JS File -->
    <script src="../assets/js/animation.js"></script>
</body>
<script>
    var talkid = <?php echo json_encode($talkid) ?>;

    loadTalkDet(talkid);
</script>

</html>