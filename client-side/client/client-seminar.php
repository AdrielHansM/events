<?php
	require_once('../../server-side/sessionHandling.php');
	$memberName = getMemberName();
	//This is where to display the seminars the member attended or will attend to
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../../javascript/loadData.js"></script>
		<!-- <link rel="stylesheet" href ="client-seminar.css"> -->
    </head>
    <body>
		<?php echo "<h1>$memberName - Talks Attended</h1>" ?>
		<table>
			<thead>
				<tr>
					<th>Talk ID</th>
					<th>Event Name</th>
					<th>Host</th>
					<th>Registration Fee</th>
					<th>Date</th>
					<th>Time Start</th>
					<th>Time End</th>
					<th>Topic</th>
					<th>Payment Status</th>
					<th>Talk Status</th>
				</tr>
			</thead>
			<tbody id="talksAttended">
			
			</tbody>
        </table>

        <button id ='back-btn' type="button" onclick="location.href = 'client-home.php';" >Back</button>
    </body>
    <script>
        loadEventHistory();
    </script>
</html>
