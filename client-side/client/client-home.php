<?php 
    require_once('../../server-side/sessionHandling.php');
    //No checkAccountType() in place because 
    //client and admin can access this page
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!-- Internal CSS -->
        <link rel="stylesheet" href="../../css/style.css">

        <!-- bootstrap CSS Files -->
        <link href="../assets/bootstrap/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/bootstrap/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
        <link href="../assets/bootstrap/glightbox/css/glightbox.min.css" rel="stylesheet">
        
        <!-- JS -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../../javascript/loadData.js"></script>
        <script src="../../javascript/logout.js"></script>        
    </head>
    <body>
<?php
    include_once("../header.html");
?>

        <!-- ======= Hero Section ======= -->
        <section id="hero">
            <div class="hero-container">
                <h1 class="mb-4 pb-0">The Annual<br><span>Marketing</span> Conference</h1>
                <p class="mb-4 pb-0">10-12 December, Downtown Conference Center, New York</p>
                <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox play-btn mb-4"></a>
                <a href="#about" class="about-btn scrollto">About The Event</a>
            </div>
        </section>
        <!-- End Hero Section -->

         <!-- ======= Events Section ======= -->
         <button class="buy-tickets" type="button" onclick="location.href = 'client-seminar.php';" >Talks Attended</button>

         <section id="events">
            <div class="container">
                <div class="section-header">
                        <h2>Upcoming Events</h2>
                        <p>Here are some of our events</p>


                        
                    <div class="row" id="talks"></div>

                </div>
            </div>
         </section>

       
        <br>
        <button type="button" id="logout" onclick="logoutToWebsite()" >Logout</button>
        <?php
    include_once("../footer.html");
?>
    </body>
    <script>
        loadSeminars("client-talks");
    </script>
</html>