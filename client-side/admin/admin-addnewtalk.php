
<?php 
require_once('../../server-side/sessionHandling.php');
//Verify if the member that logged in is an admin
checkAccountType();

?>

<html lang = 'en'>
    <head>
        <meta charset="UTF-8">
        <title>Add new Talk</title>

    </head>
    <body>
        <div>ADD NEW TALK</div>
        <div>
            <form action = "../../server-side/processors/processSeminar.php" enctype="multipart/form-data" method="post" id="productform">
                <br>


                <label for="tname">Talk Name: </label>
                <input type="text" id="tname" name="talkname" maxlength="80" required><br>

                <label for="desc">Short Description: </label>
                <input type="text" id="desc" name="shortdesc" maxlength="250" required><br>

                <label for="hname">Host Name: </label>
                <input type="text" id="hname" name="hostname" maxlength="45" required><br>

                <label for="date">Date: </label>
                <input type="date" id="date" name="talkdate" required><br>

                <label for="tstart">Time Start: </label>
                <input type="time" id="tstart" name="timestart" required><br>

                <label for="tend">Time End: </label>
                <input type="time" id="tend" name="timeend" required><br>

                <label for="fee">Registration Fee: </label>
                <input type="number" id="fee" name="regfee" step=".01" max="9999999" required><br>

                <label for="avail">Seats Available: </label>
                <input type="number" id="avail" name="seatavail"><br>
        
                <button class="add-btn" name="add">Add Event</button></br>
            </form>
            <br>

            <label for="topic">Choose the topic of talk: </label>
            <select id="topic" name="talktopic" form="productform">
                <option value="" selected disabled hidden>Choose here</option>
                <option value="Science & Technology">Science & Technology</option>
                <option value="Animals & Nature">Animals & Nature</option>
                <option value="Religion">Religion</option>
                <option value="Fitness & Wellness">Fitness & Wellness</option>
                <option value="History">History</option>
                <option value="Business">Business</option>
                <option value="Family & Relationships">Family & Relationships</option>
                <option value="Lifestyle">Lifestyle</option>
                <option value="Motivational">Motivational</option>
                <option value="Activism">Activism</option>

            </select>
            <button type="button" onclick="location.href = 'admin-home.php';" >Back</button>
             
        </div>

    </body>
</html>