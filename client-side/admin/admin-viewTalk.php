<?php 
    require_once('../../server-side/sessionHandling.php');
    //Verify if the member that logged in is an admin
     checkAccountType();
    //  $varPHP = $_GET['id'];
    //  echo $varPHP;
    
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../../javascript/loadData.js"></script>
   
    </head>
    <body>

    <?php 
        if ( isset( $_GET['talkid'] ) ) { 
          $talkid =  $_GET['talkid'];
        }  
    ?><h1><?php echo $talkid;  ?> Details</h1>
        <div class="talksDiv" id="talkDet">
       
        </div>
        <a href='update.php?talkid=<?php echo ($talkid)?>'>
        <!-- <button type='button' onclick="method()" id="update-talk">Update</button> -->
    </a>
        
    <button type='button' onclick="location.href = '../../server-side/processors/processDelete.php?id=<?php echo $talkid;?>';" id="delete-talk">Delete</button>
        <button type="button" onclick="location.href = 'admin-home.php';" >Back</button>
    </body>
    <script>
        var talkid = <?php echo json_encode($talkid)?>;
  
 
        loadTalkDet(talkid, "admin");
    </script>
</html>