<?php 
// echo "<h1>TEst</h1>";

if ( isset( $_GET['talkid'] ) ) { 
            $talkid =  $_GET['talkid'];
          $eventName =  $_GET['eventName'];
          $desc =  $_GET['desc'];
          $host =  $_GET['host'];
          $date =  $_GET['date'];
          $talkStat =  $_GET['talkStat'];
          $topic =  $_GET['topic'];

}  
?>


<html lang = 'en'>
    <head>
        <meta charset="UTF-8">
        <title>Update Talk</title>
    </head>
    <body>
        <h1>UPDATE TALK</h1>
        <div>
            <form action = "../../server-side/processors/processSeminar.php" enctype="multipart/form-data" method="post" id="productform">
                <br>


                <label for="tname">Talk Name: </label>
                <input type="text" id="tname" name="talkname" maxlength="80" required value="<?php echo $eventName;?>"><br>

                <label for="desc">Short Description: </label>
                <input type="text" id="desc" name="shortdesc" maxlength="250" required value="<?php echo $desc;?>"><br>

                <label for="hname">Host Name: </label>
                <input type="text" id="hname" name="hostname" maxlength="45" required value="<?php echo $host;?>"><br>

                <label for="date">Date: </label>
                <input type="date" id="date" name="talkdate" required><br>

                <label for="tstart">Time Start: </label>
                <input type="time" id="tstart" name="timestart" required><br>

                <label for="tend">Time End: </label>
                <input type="time" id="tend" name="timeend" required><br>

                <label for="fee">Registration Fee: </label>
                <input type="number" id="fee" name="regfee" step=".01" max="9999999" required><br>

                <label for="avail">Seats Available: </label>
                <input type="number" id="avail" name="seatavail"><br>

                <label for="talkStatus">Talk Status: </label>
                <select id="talkStatus" name="talkStatus">
                    <option value="Finished" <?php echo ($talk_status == "Finished" ? "selected" : "");?>>
                    Finished
                    </option>
                    <option value="Ongoing">Ongoing</option>
                    <option value="Upcoming">Upcoming</option>
                    <option value="Canceled" <?php echo ($talk_status == "Canceled" ? "selected" : "");?>>
                        Canceled
                    </option>
                </select><br>
        
                <button class="update-btn" name="update">Update Event</button></br>
            </form>
            <br>
                        
            
            

            <label for="topic">Choose the topic of talk: </label>
            <select id="topic" name="talktopic" form="productform">
                <option value="" selected disabled hidden>Choose here</option>
                <option value="Science & Technology">Science & Technology</option>
                <option value="Animals & Nature">Animals & Nature</option>
                <option value="Religion">Religion</option>
                <option value="Fitness & Wellness">Fitness & Wellness</option>
                <option value="History">History</option>
                <option value="Business">Business</option>
                <option value="Family & Relationships">Family & Relationships</option>
                <option value="Lifestyle">Lifestyle</option>
                <option value="Motivational">Motivational</option>
                <option value="Activism">Activism</option>

            </select>
            <!-- DI PA KO SURE -->
            <!-- <button type="button" onclick="location.href = 'admin-home.php';" >Back</button> -->
             
        </div>

    </body>
    <!-- <script>
    var talkid = <?php echo json_encode($talkid)?>;
    console.log(talkid);
    </script> -->
</html>