<?php 
    require_once('../../server-side/sessionHandling.php');
    //Verify if the member that logged in is an admin
     checkAccountType();

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../../javascript/loadData.js"></script>
        <script src="../../javascript/logout.js"></script>         
    </head>
    <body>
        <h1>Admin Module</h1>
        <button type="button" onclick="location.href = 'admin-addnewtalk.php';" >Add New Talk</button>
        <div class="talksDiv" id="talks">
       
        </div>
        <br>
        <div class="membersDiv" id="members">
            <h3>Members</h3>
        </div>
        <button type='button' onclick="logoutToWebsite()" id="logout">Logout</button>
    </body>
    <script>
        loadSeminars("admin-talks");
    </script>
</html>