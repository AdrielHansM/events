<?php 
//For session handling

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../javascript/login.js"></script>
    </head>
    <body>
        <div class="loginContainer">
            <form id="loginForm" class="loginForm">
                <input type="text" id="username" placeholder="Enter username">
                <br>
                <input type="password" id="password" placeholder="Enter password">
                <br>
                <button type="button" id="login" onclick="loginToWebsite()" >Login</button>
                <button type="button" id="register">Register</button>
            </form>
        </div>
    </body>
</html>