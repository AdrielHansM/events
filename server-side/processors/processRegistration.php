<?php
require_once('../database.php');

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$contact = $_POST['contact'];
$username = $_POST['username'];
$password = $_POST['password'];
$email = $_POST['email'];
$address = $_POST['address'];

//Insert query
$query = 
    "INSERT INTO 
        seminar.members (first_name, last_name, contact_no, account_username, account_password, email, member_address) 
    VALUES 
        (?, ?, ?, ?, ?, ?, ?)";

$stmtselect = $database->prepare($query);
$stmtselect->execute([$firstName, $lastName, $contact, $username, $password, $email, $address]);

echo "Successfully registered";