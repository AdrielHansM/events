<?php
    require_once('../database.php');
    require_once('../../server-side/sessionHandling.php');

//Current user's ID
    $memberId = getMemberID();
//ID of the event
    $payStat = 'Paid';

    $talkdetID = isset($_GET['talkdetid']) ? $_GET['talkdetid']: '';

    try{

        $sql = "UPDATE events.talk_details 
                SET talk_details.seat_available = talk_details.seat_available - 1, 
                talk_details.seat_reserved = talk_details.seat_reserved + 1 
                WHERE talk_details.talk_det_id = '$talkdetID' ";

        $statement = $database->prepare($sql);
            
        $database->exec($sql);

        $sql = "INSERT INTO events.participants (participants.member_id, participants.talk_det_id, participants.payment_status) 
                VALUES (:mem_id, :talkdetid, :paystat)";

        $statement = $database->prepare($sql);  

        $statement->execute([
            ':mem_id' => $memberId,
            ':talkdetid' => $talkdetID,
            ':paystat' => $payStat
        ]);
        
        echo "<script>
        alert('You successfully joined');
        window.location.href= '../../client-side/client/client-home.php';
        </script>";
    }catch(PDOException $e){
        echo $sql . "<br>" . $e->getMessage();
    }   
    
//Automatically set the status to paid
// $payStat = 'Paid';


 
// //Insert query
// $query1 = "";

// //Prepared query
// $stmtinsert = $database->prepare($query1);
// $stmtinsert->execute([$memberId, $talkId, $payStat]);

// //Update query
// $query2 = 
//     "UPDATE talk_details
//     SET seat_available = seat_available - 1, 
//     seat_reserved = seat_reserved + 1 
//     WHERE talk_id = $talkId";

// //Prepared query2
// $stmtupdate = $database->prepare($query2);
// $stmtupdate->execute([$talkId]);

// echo "Successful Registration";

// $query1 = 
// "INSERT INTO 
//         participants (`member_id`, `talk_det_id`, `payment_status`) 
//     VALUES (?, ?, ?)";
?>