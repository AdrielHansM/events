<?php
require_once('../database.php');

//A post http request should be sent for this to work
$username = $_POST['username'];
$password = $_POST['password'];

//Test variables
// $username = "gGenove";
// $password = "member1";

$query =
    "SELECT 
        * 
    FROM 
        events.members
    WHERE
        members.account_username = ?
    ";

$stmtselect = $database->prepare($query);
$stmtselect->execute([$username]);

$loginDetails = $stmtselect->fetch(PDO::FETCH_ASSOC);

//Check if the hash of the password entered  
//and the saved passord is equal
if(password_verify($password, $loginDetails['account_password'])){

    //If it is equal, then set the session for session handling
    //For storing $_SESSION credentials
    session_start();
    $_SESSION['loginCredentials'] = $loginDetails;

    //Uncomment to check session credentials
    //var_dump($_SESSION);
    echo $loginDetails['account_type'];
} else {
    throw new Exception(header('HTTP/1.0 400 Invalid username or password'));
}