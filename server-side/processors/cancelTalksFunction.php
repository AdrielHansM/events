<?php
require_once('../database.php');
require_once('../../server-side/sessionHandling.php');

$memberName = getMemberName();
$memberID = getMemberID();
$paymentStat = 'Refunded';

if(isset($_POST['cancel'])) {
    $talk_id = $_POST['talkID'][0];

    $query = "UPDATE events.talk_details
                INNER JOIN events.participants 
                ON talk_details.talk_det_id = participants.talk_det_id 
                SET seat_available = seat_available + 1, seat_reserved = seat_reserved - 1, participants.payment_status = :paymentStat 
                WHERE participants.member_id = :memberID AND talk_details.talk_id = :talkID";
    
        $statement = $database->prepare($query);
    
        $statement->execute([
            ':paymentStat'=>$paymentStat,
            ':memberID'=>$memberID,
            ':talkID'=>$talk_id
        ]);
    }
    echo "<script>
        alert('Sucessfully cancelled participation');
        window.location.href= '../../client-side/client/client-seminar.php';
        </script>";


?>
