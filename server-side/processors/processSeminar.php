<?php
//TODO
//Update seminar
//Add talk_event
require_once('../database.php');
 include("../../client-side/admin/admin-addnewtalk.php");

    if(isset($_POST['add'])){   
        $event_name=$_POST['talkname'];
        $short_desc=$_POST['shortdesc'];
        $host=$_POST['hostname'];
        $talk_date=$_POST['talkdate'];
        $time_start=$_POST['timestart'];
        $time_end=$_POST['timeend'];
        $registration_fee=$_POST['regfee'];
        $seat_available=$_POST['seatavail'];
        $selected=$_POST['talktopic'];

        $sql = 'INSERT INTO events.talks(event_name, short_desc, host, date, time_start, time_end, topic) VALUES(:event_name, :short_desc, :host, :date, :time_start, :time_end, :topic)';

        $statement = $database->prepare($sql);

        $statement->execute([
            ':event_name' => $event_name,
            ':short_desc' => $short_desc,
            ':host' => $host,
            ':date' => $talk_date,
            ':time_start' => $time_start,
            ':time_end' => $time_end,
            ':topic' => $selected
        ]);

        $last_id = $database->lastInsertId();

        $sql = 'INSERT INTO events.talk_details(registration_fee, seat_available, talk_id) VALUES (:registration_fee, :seat_available, :talk_id)';
    
        $statement = $database->prepare($sql);

        $statement->execute([
            ':registration_fee' => $registration_fee,
            ':seat_available' => $seat_available,
            ':talk_id' => $last_id
        ]);

        echo "<script>
        alert('Talk successfully added');
        window.location.href= '../../client-side/admin/admin-home.php';
        </script>";

        //uncomment to redirect back to admin-home.php
        //header("Location: ../../client-side/admin/admin-home.php");

    }
//Delete Seminar