<?php
    require_once('../database.php');

        $id = isset($_GET['id']) ? $_GET['id'] : '';
        try { 
            $sql = "DELETE events.talks , events.talk_details , events.participants
                    FROM events.talks
                    INNER JOIN events.talk_details ON talks.talk_id = talk_details.talk_id
                    INNER JOIN events.participants ON talk_details.talk_det_id = participants.talk_det_id 
                    AND talks.talk_id = '$id'"; 

            $statement = $database->prepare($sql);
            
            $database->exec($sql);

            $sql = "DELETE events.talks , events.talk_details 
                    FROM events.talks
                    INNER JOIN events.talk_details ON talks.talk_id = talk_details.talk_id
                    WHERE talks.talk_id = '$id'"; 

            $statement = $database->prepare($sql);

            $database->exec($sql);

            
        } 

        catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
        echo "<script>
        alert('Talk successfully deleted');
        window.location.href= '../../client-side/admin/admin-home.php';
        </script>";
       // header("Location: ../../client-side/admin/admin-home.php");
?>