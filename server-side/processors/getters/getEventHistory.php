<?php
require_once('../database.php');
require_once('../../server-side/sessionHandling.php');
$member_id = getMemberID();

//Query to get the event history
$query = "SELECT 
			talks.talk_id, 
			talks.event_name, 
			talks.host, 
			talk_details.registration_fee, 
			talks.date, 
			talks.time_start, 
			talks.time_end, 
			talks.topic, 
			participants.payment_status 
		FROM 
			events.participants 
		INNER JOIN 
			events.talk_details
		ON 
			participants.talk_det_id = talk_details.talk_det_id
		INNER JOIN 
			events.talks
		ON 
			talk_details.talk_id = talks.talk_id
		WHERE 
			member_id = $member_id AND talk_status = 'finished'";

//Prepared query for efficient queries and security for sql injection attacks
$stmtselect = $database->prepare($query);
$stmtselect->execute();

//Initalize results variable
$results = [];

//While there are still rows, every row will be inserted in results array
while($row = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row;
}

//Uncomment var_dump if you want to see the structure of the JSON response
//var_dump($results);

//returns a json response
echo json_encode($results);