<?php
require_once('../database.php');

$query = 
    "SELECT
        *
    FROM
        talks.members
    ";

$stmtselect = $database->prepare($query);
$stmtselect->execute();

$results = array();

while($row = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row;
}

//var_dump($results);
echo json_encode($results);