<?php

session_start();

$acc_type = "mapapalitan";

if(!isset($_SESSION['loginCredentials'])) {
    header("Location:../index.php");
}

//Checks if the member logged in is an admin
//If not, then it redirects back to client home page
function checkAccountType() {
    //Accessing session credentials
    $credentials = $_SESSION['loginCredentials'];

    // global $acc_type;
    // $acc_type = $credentials['account_type'];
 
    

    if($credentials['account_type'] != 'admin') {
        header("Location:../client/client-home.php");
    }
}

//Returns the id of the logged in member
function getMemberID() {
    $credentials = $_SESSION['loginCredentials'];
	return $credentials['member_id'];
}

//Returns the name of the logged in member
function getMemberName() {
    $credentials = $_SESSION['loginCredentials'];
	return $credentials['first_name'] . " " . $credentials['last_name'];
}
?>
