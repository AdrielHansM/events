<?php
require_once('../database.php');

$talkid = $_GET['talkid'];


//Query to get the seminar and seminar details 
$query = 
    "SELECT
        talks.talk_id,
        talks.event_name,
        talks.short_desc,
        talks.host,
        talks.date,
        talks.time_start,
        talks.time_end,
        talks.topic,
        talk_details.talk_det_id,
        talk_details.registration_fee,
        talk_details.talk_status,
        talk_details.seat_available,
        talk_details.seat_reserved
    FROM
        events.talks
    JOIN
        events.talk_details
    ON
        talks.talk_id = talk_details.talk_id
    WHERE 
        talks.talk_id = $talkid;
    ";

//Prepared query for efficient queries and security for sql injection attacks
$stmtselect = $database->prepare($query);
$stmtselect->execute();

//Initalize results variable
$results = [];

//While there are still rows, every row will be inserted in results array
while($row = $stmtselect->fetch(PDO::FETCH_ASSOC)) {
    $results[] = $row;
}

//Uncomment var_dump if you want to see the structure of the JSON response
//var_dump($results);

//returns a json response
echo json_encode($results);