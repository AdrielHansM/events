var talkID;
var talksjson;
//Send an http get response to a backend php file (getSeminars.php)
//You can reuse this method to load the list of members
async function loadSeminars(acc_type) {
    $.ajax({
        method: 'GET',
        url: '../../server-side/getters/getSeminars.php',
        
        success: function(results) {
    
            var json = jQuery.parseJSON(results);
            talksjson = json;
            //NOTE: uncomment if you want to see the json and its length
            console.log(json);
            console.log
            if(acc_type == 'client-talks'){
                displaySeminarClient(json);
            }else{
                displaySeminarAdmin(json);
            };

          
        }
    });
}

//Display the response in an html page for client side
async function displaySeminarClient(json) {
    var div = document.getElementById("talks");

    for (let i = 0; i < json.length; i++) {
        var id = json[i]['talk_id'];
        var eventName = json[i]['event_name'];
        //var desc = json[i]['short_desc'];
        //var host = json[i]['host'];
        var date = json[i]['date'];
        // var seatA = json[i]['seat_available'];
        // var seatR = json[i]['seat_reserved'];

        div.innerHTML += 
        `
        <div class="col-lg-4 col-md-6" id="${id}">
            <div class="speaker">
                <img src="../assets/img/speakers/1.jpg" alt="Speaker 1" class="img-fluid">
                <div class="details">
                    <p>${eventName}</p>
                    <p><strong>Date:</strong> ${date}</p>

                    <button class="buy-tickets" id="${id}" onClick=cliViewTalk(${id}) value="${id}" type='reset'>View Talk</button><br>
                </div>
            </div>
        </div>
        `;
    }
}

//Display the response in an html page for admin side
async function displaySeminarAdmin(json) {
    var div = document.getElementById("talks");

    for (let i = 0; i < json.length; i++) {
        var id = json[i]['talk_id'];
        var eventName = json[i]['event_name'];
        var desc = json[i]['short_desc'];
        var host = json[i]['host'];
        var date = json[i]['date'];
        var seatA = json[i]['seat_available'];
        var seatR = json[i]['seat_reserved'];
        var talkStat = json[i]['talk_status'];
        var topic = json[i]['topic'];


        div.innerHTML += 
        `
        <div id="${id}">
        <p><strong>Event Name:</strong> ${eventName}</p>
        <p><strong>Event Desc:</strong> ${desc}</p>
        <p><strong>Host:</strong> ${host}</p>
        <p><strong>Date:</strong> ${date}</p>
        <p><strong>Seats Available:</strong> ${seatA}</p>
        <p><strong>Seats Reserved:</strong> ${seatR}</p>
        <p><strong>Talk Status:</strong> ${talkStat}</p>
        <button id="${id}" onClick=redViewTalk(${id}) value="${eventName}">View Talk</button>
        </div><br>  
        `;
    }
}
//${id},${eventName},${desc},${host},${date},${talkStat},${topic}

//Send an http get response to a backend php file (getEventHistory.php)
async function loadEventHistory() {
    $.ajax({
        method: 'GET',
        url: '../../server-side/getters/getEventHistory.php',
        success: function(results) {
            var json = jQuery.parseJSON(results)

            //NOTE: uncomment if you want to see the json and its length
            console.log(json);

            displayEventHistory(json);
        }
    });
}

//Display the talks attended in an html page
async function displayEventHistory(json) {
    var tbody = document.getElementById("talksAttended");

    for (let i = 0; i < json.length; i++) {
        var id = json[i]['talk_id'];

        var eventName = json[i]['event_name'];
        var host = json[i]['host'];
        var regFee = json[i]['registration_fee'];
        var date = json[i]['date'];
        var timeStart = json[i]['time_start'];
        var timeEnd = json[i]['time_end'];
		var topic = json[i]['topic'];
		var paymentStat = json[i]['payment_status'];
        var talkStatus = json[i]['talk_status'];

        tbody.innerHTML += 
        `
        <tr>
			<td name= $talk_id>${id}</td>
			<td>${eventName}</td>
			<td>${host}</td>
			<td>${regFee}</td>
			<td>${date}</td>
			<td>${timeStart}</td>
			<td>${timeEnd}</td>
			<td>${topic}</td>
			<td>${paymentStat}</td>
            <td>${talkStatus}</td>
            <td> <form method="post" action ='../../server-side/processors/cancelTalksFunction.php'>
                <input id =cancel${id} class = 'cancel-btn' type = "submit" alt=${id} name = 'cancel' value ="Cancel">
                <input type ='hidden' name ='talkID[]' value=${id}>
                </form>
            </td>
		</tr>
        `;
        console.log('cancel' + id);
        // console.log((talkStatus === 'Finished' || talkStatus === 'Canceled'));
     if(talkStatus === 'Canceled' || talkStatus === 'Finished' || paymentStat === 'Refunded'){
        document.getElementById('cancel'+id).style.visibility = 'hidden';
        console.log("hide"); 
              
     }else{
        console.log("not");
     }
        // if(talkStatus == 'Canceled' || talkStatus == 'Finished'){
            
            
        //     console.log("here1");
        // } else{
        //     document.getElementById().style.visibility = 'hidden';
        //     console.log("here2");
        // }
    }
    // document.getElementById('cancel-btn').style.visibility = 'hidden';
    
}
async function bookTicket(id) {
    console.log('ticket: ' + id + ' is booked');
}

async function cliViewTalk(id) {  
    // console.log(id);
    window.location.href = 'client-viewTalk.php?talkid=' + id;
    return false;
}


async function redViewTalk(id) {  
     console.log(id);
    //  console.log(eventName);
    //  console.log(desc);
    //  console.log(host);
    //  console.log(date);
    //  console.log(talkStat);
    //  console.log(topic);
    window.location.href = 'admin-viewTalk.php?talkid=' + id;

    //  window.location.href = 'admin-home.php?talkid=asdfadfasdfas' + eventName;
}

async function loadTalkDet(talkid, acc_type) {
    console.log(talkid);
    $.ajax({
        method: 'GET',
        url: '../../server-side/getters/getTalkDetails.php',
        data: {talkid: talkid},
        success: function(results){
            var json = jQuery.parseJSON(results);

            console.log(json);

            if(acc_type == "admin"){
                displayTalkDetails(json);
            }else{
                displayTalkDetailsClient(json)
            }
            

        }
    });
}

async function displayTalkDetails(json){
    var div = document.getElementById("talkDet");

    var arr = json[0];

    var id = arr['talk_id'];
    console.log(id);
    var talkDetID = arr['talk_det_id'];
    var eventName = arr['event_name'];
    var desc = arr['short_desc']; 
    var topic = arr['topic'];
    console.log(topic);
    var host = arr['host'];
    var date = arr['date'];
    var timeStart = arr['time_start'];
    var timeEnd = arr['time_end'];
    var regFee = arr['registration_fee'];
    var seatA = arr['seat_available'];
    var seatR = arr['seat_reserved'];
    var talkStat = arr['talk_status'];

    div.innerHTML += 
        `
        <form action = "../../server-side/processors/processUpdate2.php" enctype="multipart/form-data" method="post" id="updateform">
                <br>
                <input type="hidden" name="talkdetid" value="${talkDetID}">
                <input type="hidden" name="talkid[]" value="${id}">
                <label for="tname">Talk Name: </label>
                <input type="text" id="tname" name="talkname" maxlength="80" required value= "${eventName}" class = "disabled"><br>

                <label for="desc">Short Description: </label>
                <input type="text" id="desc" name="shortdesc" maxlength="250" required value= "${desc}" size = 60 class = "disabled"><br>

                <label for="currtopic">Current Topic: </label>
                <input type="text" id="currtopic[]" value = "${topic}" disabled>
                <input type="hidden" id="currtopic[]" name="currtopic" value = "${topic}" ><br>

                
                <label for="topic" id="label1">Update Topic of the talk: </label>
                <select id="topic" name="talktopic" class="hide">
                    <option value="" selected>Choose here</option>
                    <option value="Science & Technology">Science & Technology</option>
                    <option value="Animals & Nature">Animals & Nature</option>
                    <option value="Religion">Religion</option>
                    <option value="Fitness & Wellness">Fitness & Wellness</option>
                    <option value="History">History</option>
                    <option value="Business">Business</option>
                    <option value="Family & Relationships">Family & Relationships</option>
                    <option value="Lifestyle">Lifestyle</option>
                    <option value="Motivational">Motivational</option>
                    <option value="Activism">Activism</option>
                </select>
                <br>

                <label for="hname">Host Name: </label>
                <input type="text" id="hname" name="hostname" maxlength="45" required value= "${host}" class = "disabled"><br>

                <label for="date">Date: </label>
                <input type="date" id="date" name="talkdate" required value= "${date}" class = "disabled"><br>

                <label for="tstart">Time Start: </label>
                <input type="time" id="tstart" name="timestart" required value= "${timeStart}" class = "disabled"><br>

                <label for="tend">Time End: </label>
                <input type="time" id="tend" name="timeend" required value= "${timeEnd}" class = "disabled"><br>

                <label for="currstatus">Current talk status: </label>
                <input type="text" id="currstatus[]" value = "${talkStat}" disabled>
                <input type="hidden" id="currstatus[]" name="currstatus" value = "${talkStat}" ><br>

                <label for="statustalk" id="label2">Choose talk Status: </label>
                <select id="statustalk" name="statustalk" class= "hide">
                    <option value="" selected>Choose here</option>
                    <option value="Finished">Finished</option>
                    <option value="Ongoing">Ongoing</option>
                    <option value="Upcoming">Upcoming</option>
                    <option value="Canceled">Canceled</option>
                </select><br>

                <label for="fee">Registration Fee: </label>
                <input type="number" id="fee" name="regfee" step=".01" max="9999999" required value= "${regFee}" class = "disabled"><br>

                <label for="avail">Seats Available: </label>
                <input type="number" id="avail" name="seatavail" value = "${seatA}" disabled><br>

                <label for="res">Seats Reserved: </label>
                <input type="number" id="avail" name="seatreserved" value = "${seatR}" disabled><br>

                
        
                <button class="update-btn" id="up-btn" name="update">Update</button></br>
            </form>

        `;

    if(talkStat == 'Upcoming' || talkStat == 'Ongoing'){
        document.getElementById('delete-talk').style.visibility = 'hidden';
    }

    console.log(talkStat != 'Upcoming');
    if(talkStat != 'Upcoming'){
        document.getElementById('up-btn').style.visibility = 'hidden';
        var toDisable = document.getElementsByClassName('disabled');

        for (let i = 0; i < toDisable.length; i++){
            toDisable[i].disabled = true;
        }

        document.getElementById("statustalk").style.visibility = 'hidden';
        document.getElementById("topic").style.visibility = 'hidden';
        document.getElementById("label1").style.visibility = 'hidden';
        document.getElementById("label2").style.visibility = 'hidden';
        // for (let j = 0; j < toHide.length; j++){
        //     toDisable[j].style.visibility = 'hidden';
        // }
    }

    // <div id="${id}">
    // <h2>${eventName}</h2>
    // <h3>${desc}</h3>
    // <p><strong>Topic:</strong> ${topic}</p>
    // <p><strong>Host:</strong> ${host}</p>
    // <p><strong>Date:</strong> ${date}</p>
    // <p><strong>Time Start:</strong> ${timeStart}</p>
    // <p><strong>Time End:</strong> ${timeEnd}</p>
    // <p><strong>Registration Fee:</strong> ${regFee}</p>
    // <p><strong>Seats Available:</strong> ${seatA}</p>
    // <p><strong>Talk Status:</strong> ${talkStat}</p>
    // </div><br>  
    /*
    <label for="talkStatus">Talk Status: </label>
                <select id="talkStatus" name="talkStatus">
                    <option value="Finished" <?php echo ($talk_status == "Finished" ? "selected" : "");?>>
                    Finished
                    </option>
                    <option value="Ongoing">Ongoing</option>
                    <option value="Upcoming">Upcoming</option>
                    <option value="Canceled" <?php echo ($talk_status == "Canceled" ? "selected" : "");?>>
                        Canceled
                    </option>
                </select><br>
    */
}

async function displayTalkDetailsClient(json){
    var div = document.getElementById("talkDet");

    var arr = json[0];

    var id = arr['talk_id'];
    console.log(id);
    var talkDetID = arr['talk_det_id'];
    console.log(talkDetID);
    var eventName = arr['event_name'];
    var desc = arr['short_desc']; 
    var topic = arr['topic'];
    console.log(topic);
    var host = arr['host'];
    var date = arr['date'];
    var timeStart = arr['time_start'];
    var timeEnd = arr['time_end'];
    var regFee = arr['registration_fee'];
    var seatA = arr['seat_available'];

    var talkStat = arr['talk_status'];

    div.innerHTML += 
        `
        <div id="${id}">
        <h2>${eventName}</h2>
        <h3>${desc}</h3>
        <p><strong>Topic:</strong> ${topic}</p>
        <p><strong>Host:</strong> ${host}</p>
        <p><strong>Date:</strong> ${date}</p>
        <p><strong>Time Start:</strong> ${timeStart}</p>
        <p><strong>Time End:</strong> ${timeEnd}</p>
        <p><strong>Registration Fee:</strong> ${regFee}</p>
        <p><strong>Seats Available:</strong> ${seatA}</p>
        <p><strong>Talk Status:</strong> ${talkStat}</p>
        <button type='button' onclick="location.href = '../../server-side/processors/processSignUp.php?talkdetid=${talkDetID}';" id="sign-up">Sign Up</button>
        <button type="button" onclick="location.href = 'client-home.php';" >Back</button>
        </div>
        `;
/* <div id="${id}">
    <h2>${eventName}</h2>
    <h3>${desc}</h3>
    <p><strong>Topic:</strong> ${topic}</p>
    <p><strong>Host:</strong> ${host}</p>
    <p><strong>Date:</strong> ${date}</p>
    <p><strong>Time Start:</strong> ${timeStart}</p>
    <p><strong>Time End:</strong> ${timeEnd}</p>
    <p><strong>Registration Fee:</strong> ${regFee}</p>
    <p><strong>Seats Available:</strong> ${seatA}</p>
    <p><strong>Talk Status:</strong> ${talkStat}</p>
    </div><br>   */
}