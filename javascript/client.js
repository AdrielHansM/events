async function load() {
    $.ajax({
        method: 'POST',
        url: '../../server-side/getters/getSeminars.php',
        success: function(results) {
            var json = jQuery.parseJSON(results)

            //NOTE: uncomment if you want to see the json and its length
            console.log(json);
            // console.log(json.length);

            displaySeminar(json);
        }
    });
}

async function displaySeminar(json) {
    var div = document.getElementById("talks");

    for (let i = 0; i < json.length; i++) {
        var id = json[i]['talk_id'];
        var eventName = json[i]['event_name'];
        var desc = json[i]['short_desc'];
        var host = json[i]['host'];
        var date = json[i]['date'];
        var seatA = json[i]['seat_available'];
        var seatR = json[i]['seat_reserved'];

        div.innerHTML += 
        `
        <div id="${id}">
        <p><strong>Event Name:</strong> ${eventName}</p>
        <p><strong>Event Desc:</strong> ${desc}</p>
        <p><strong>Host:</strong> ${host}</p>
        <p><strong>Date:</strong> ${date}</p>
        <p><strong>Seats Available:</strong> ${seatA}</p>
        <p><strong>Seats Reserved:</strong> ${seatR}</p>
        <button id="${id}" onClick= bookTicket(${id}) value="${id}">Buy Ticket</button>
        </div><br>
        `;
    }
}

async function bookTicket(id) {
    console.log('ticket: ' + id + ' is booked');
}