<?php 

    class Talk {

        private $talk_id;
        private $event_name;
        private $short_desc;
        private $host;
        private $date; 
        private $time_start;
        private $time_end;
        private $topic;
        private $talk_det_id;
        private $registration_fee;
        private $talk_status;
        private $seat_available;
        private $seat_reserved;

        public function __construct($talk_id, $event_name, $short_desc, $host, $date, $time_start, $time_end, $topic, $talk_det_id, $registration_fee, $talk_status, $seat_available, $seat_reserved){
            $this->talk_id = $talk_id;
            $this->event_name = $event_name;
            $this->short_desc = $short_desc;
            $this->host = $host;
            $this->date = $date;
            $this->time_start = $time_start;
            $this->time_end = $time_end;
            $this->topic = $topic;
            $this->talk_det_id = $talk_det_id;
            $this->registration_fee = $registration_fee;
            $this->talk_status = $talk_status;
            $this->seat_available = $seat_available;
            $this->seat_reserved = $seat_reserved;
       
        }




        /**
         * Get the value of talk_id
         */ 
        public function getTalk_id()
        {
                return $this->talk_id;
        }

        /**
         * Set the value of talk_id
         *
         * @return  self
         */ 
        public function setTalk_id($talk_id)
        {
                $this->talk_id = $talk_id;

                return $this;
        }

        /**
         * Get the value of event_name
         */ 
        public function getEvent_name()
        {
                return $this->event_name;
        }

        /**
         * Set the value of event_name
         *
         * @return  self
         */ 
        public function setEvent_name($event_name)
        {
                $this->event_name = $event_name;

                return $this;
        }

        /**
         * Get the value of short_desc
         */ 
        public function getShort_desc()
        {
                return $this->short_desc;
        }

        /**
         * Set the value of short_desc
         *
         * @return  self
         */ 
        public function setShort_desc($short_desc)
        {
                $this->short_desc = $short_desc;

                return $this;
        }

        /**
         * Get the value of host
         */ 
        public function getHost()
        {
                return $this->host;
        }

        /**
         * Set the value of host
         *
         * @return  self
         */ 
        public function setHost($host)
        {
                $this->host = $host;

                return $this;
        }

        /**
         * Get the value of date
         */ 
        public function getDate()
        {
                return $this->date;
        }

        /**
         * Set the value of date
         *
         * @return  self
         */ 
        public function setDate($date)
        {
                $this->date = $date;

                return $this;
        }

        /**
         * Get the value of time_start
         */ 
        public function getTime_start()
        {
                return $this->time_start;
        }

        /**
         * Set the value of time_start
         *
         * @return  self
         */ 
        public function setTime_start($time_start)
        {
                $this->time_start = $time_start;

                return $this;
        }

        /**
         * Get the value of topic
         */ 
        public function getTopic()
        {
                return $this->topic;
        }

        /**
         * Set the value of topic
         *
         * @return  self
         */ 
        public function setTopic($topic)
        {
                $this->topic = $topic;

                return $this;
        }

        /**
         * Get the value of time_end
         */ 
        public function getTime_end()
        {
                return $this->time_end;
        }

        /**
         * Set the value of time_end
         *
         * @return  self
         */ 
        public function setTime_end($time_end)
        {
                $this->time_end = $time_end;

                return $this;
        }

        /**
         * Get the value of registration_fee
         */ 
        public function getRegistration_fee()
        {
                return $this->registration_fee;
        }

        /**
         * Set the value of registration_fee
         *
         * @return  self
         */ 
        public function setRegistration_fee($registration_fee)
        {
                $this->registration_fee = $registration_fee;

                return $this;
        }

        /**
         * Get the value of talk_status
         */ 
        public function getTalk_status()
        {
                return $this->talk_status;
        }

        /**
         * Set the value of talk_status
         *
         * @return  self
         */ 
        public function setTalk_status($talk_status)
        {
                $this->talk_status = $talk_status;

                return $this;
        }

        /**
         * Get the value of seat_available
         */ 
        public function getSeat_available()
        {
                return $this->seat_available;
        }

        /**
         * Set the value of seat_available
         *
         * @return  self
         */ 
        public function setSeat_available($seat_available)
        {
                $this->seat_available = $seat_available;

                return $this;
        }

        /**
         * Get the value of seat_reserved
         */ 
        public function getSeat_reserved()
        {
                return $this->seat_reserved;
        }

        /**
         * Set the value of seat_reserved
         *
         * @return  self
         */ 
        public function setSeat_reserved($seat_reserved)
        {
                $this->seat_reserved = $seat_reserved;

                return $this;
        }

        /**
         * Get the value of talk_det_id
         */ 
        public function getTalk_det_id()
        {
                return $this->talk_det_id;
        }

        /**
         * Set the value of talk_det_id
         *
         * @return  self
         */ 
        public function setTalk_det_id($talk_det_id)
        {
                $this->talk_det_id = $talk_det_id;

                return $this;
        }
    }

?>

//